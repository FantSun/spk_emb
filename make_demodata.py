import os
import pickle
import numpy as np

rootDir = './spmel/valid_b8'
tarFile = 'demo.pkl'
dirName, subdirList, _ = next(os.walk(rootDir))
print('Found directory: %s' % dirName)

speakers = []
for speaker in sorted(subdirList):
    print('Processing speaker: %s' % speaker)
    utterances = []
    #utterances.append(speaker)
    _, _, fileList = next(os.walk(os.path.join(dirName,speaker)))
    
    # create file list
    for fileName in sorted(fileList):
        utterances.append(np.load(os.path.join(rootDir, speaker, fileName)))
    speakers.append(utterances)
    
with open(os.path.join(rootDir, tarFile), 'wb') as handle:
    pickle.dump(speakers, handle)    
