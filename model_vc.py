import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import random


class LinearNorm(torch.nn.Module):
    def __init__(self, in_dim, out_dim, bias=True, w_init_gain='linear'):
        super(LinearNorm, self).__init__()
        self.linear_layer = torch.nn.Linear(in_dim, out_dim, bias=bias)

        torch.nn.init.xavier_uniform_(
            self.linear_layer.weight,
            gain=torch.nn.init.calculate_gain(w_init_gain))

    def forward(self, x):
        return self.linear_layer(x)


class ConvNorm(torch.nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=1, stride=1,
                 padding=None, dilation=1, bias=True, w_init_gain='linear'):
        super(ConvNorm, self).__init__()
        if padding is None:
            assert(kernel_size % 2 == 1)
            padding = int(dilation * (kernel_size - 1) / 2)

        self.conv = torch.nn.Conv1d(in_channels, out_channels,
                                    kernel_size=kernel_size, stride=stride,
                                    padding=padding, dilation=dilation,
                                    bias=bias)

        torch.nn.init.xavier_uniform_(
            self.conv.weight, gain=torch.nn.init.calculate_gain(w_init_gain))

    def forward(self, signal):
        conv_signal = self.conv(signal)
        return conv_signal


class ConvNorm_2d(torch.nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=1, stride=1,
                 padding=None, dilation=1, bias=True, w_init_gain='linear'):
        super(ConvNorm_2d, self).__init__()
        if padding is None:
            assert(kernel_size % 2 == 1)
            padding = int(dilation * (kernel_size - 1) / 2)

        self.conv = torch.nn.Conv2d(in_channels, out_channels,
                                    kernel_size=kernel_size, stride=stride,
                                    padding=padding, dilation=dilation,
                                    bias=bias)

        torch.nn.init.xavier_uniform_(
            self.conv.weight, gain=torch.nn.init.calculate_gain(w_init_gain))

    def forward(self, signal):
        conv_signal = self.conv(signal)
        return conv_signal


class Encoder(nn.Module):
    """Encoder module:
    """
    def __init__(self, dim_emb, len_min):
        super(Encoder, self).__init__()
        self.dim_emb = dim_emb
        self.len_min = len_min
        self.cha_input = self.len_min
        self.len_inlinear = self.len_min // 8 * 80
        #self.cha_input = 80
        #self.len_inlinear = 80 // 8 * self.len_min
        
        convolutions = []
        for i in range(3):
            conv_layer = nn.Sequential(
                ConvNorm(self.cha_input if i==0 else 256, # 128 or 80
                         self.cha_input//8 if i == 2 else 256,
                         kernel_size=5, stride=1,
                         padding=2,
                         dilation=1, w_init_gain='relu'),
                nn.BatchNorm1d(self.cha_input//8 if i == 2 else 256))
            convolutions.append(conv_layer)
        self.convolutions = nn.ModuleList(convolutions)

        convolutions_2d = []
        for i in range(3):
            conv_layer_2d = nn.Sequential(
                ConvNorm_2d(1 if i==0 else 8,
                         1 if i==2 else 8,
                         kernel_size=5, stride=1,
                         padding=2,
                         dilation=1, w_init_gain='relu'),
                nn.BatchNorm2d(1 if i == 2 else 8))
            convolutions_2d.append(conv_layer_2d)
        self.convolutions_2d = nn.ModuleList(convolutions_2d)

        self.linear_projection = LinearNorm(self.len_inlinear, self.dim_emb)

    def forward(self, x):
        x = x.squeeze(1)
        #x = x.squeeze(1).transpose(2,1)
        
        for conv in self.convolutions:
            x = F.relu(conv(x))
        #x = x.transpose(1, 2)
        x = x.unsqueeze(1)

        for conv in self.convolutions_2d:
            x = F.relu(conv(x))

        emb = self.linear_projection(x.reshape(x.shape[0], -1))
        
        return emb
      
        
class Decoder(nn.Module):
    """Decoder module:
    """
    def __init__(self, dim_emb, dim_pre):
        super(Decoder, self).__init__()
        
        self.lstm1 = nn.LSTM(dim_emb, dim_pre, 1, batch_first=True)
        
        convolutions = []
        for i in range(3):
            conv_layer = nn.Sequential(
                ConvNorm(dim_pre,
                         dim_pre,
                         kernel_size=5, stride=1,
                         padding=2,
                         dilation=1, w_init_gain='relu'),
                nn.BatchNorm1d(dim_pre))
            convolutions.append(conv_layer)
        self.convolutions = nn.ModuleList(convolutions)
        
        self.lstm2 = nn.LSTM(dim_pre, 1024, 2, batch_first=True)
        
        self.linear_projection = LinearNorm(1024, 80)

    def forward(self, x):
        
        #self.lstm1.flatten_parameters()
        x, _ = self.lstm1(x)
        x = x.transpose(1, 2)
        
        for conv in self.convolutions:
            x = F.relu(conv(x))
        x = x.transpose(1, 2)
        
        outputs, _ = self.lstm2(x)
        
        decoder_output = self.linear_projection(outputs)

        return decoder_output   
    
    

class Generator(nn.Module):
    """Generator network."""
    def __init__(self, dim_emb, dim_pre, len_min):
        super(Generator, self).__init__()
        
        self.encoder = Encoder(dim_emb, len_min)
        self.decoder = Decoder(dim_emb, dim_pre)

    def forward(self, x):
                
        emb = self.encoder(x)
        emb_resampling = emb + torch.randn(emb.shape).to(emb.device)

        emb_decoder = emb_resampling.unsqueeze(1).expand(-1,x.size(1),-1)

        mel_outputs = self.decoder(emb_decoder)
                
        return mel_outputs, emb


def RCR_segment(x, seg_min=9, seg_max=24):
    num_max = x.shape[1] // seg_min + 1
    len_seg = torch.randint(seg_min, seg_max+1, (len(x),num_max,))
    cum_seg = torch.cumsum(len_seg, dim=1)
    x_out = []
    for i in range(len(x)):
        xi_split = list(torch.tensor_split(x[i], cum_seg[i]))
        random.shuffle(xi_split)
        xi_rcr = torch.cat(xi_split, dim=0)
        x_out.append(xi_rcr.unsqueeze(0))
    x_out = torch.cat(x_out, dim=0)
    return x_out

def RCR_frame(x):
    x_out = []
    for i in range(len(x)):
        ord_rcr = torch.randperm(x.shape[1])
        xi_rcr = x[i][ord_rcr]
        x_out.append(xi_rcr.unsqueeze(0))
    x_out = torch.cat(x_out, dim=0)
    return x_out
