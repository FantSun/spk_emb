import os
import argparse
from solver_encoder import Solver
from data_loader import get_loader
from torch.backends import cudnn


def str2bool(v):
    return v.lower() in ('true')

def main(config):
    # For fast training.
    cudnn.benchmark = True

    # Create directories if not exist.
    if not os.path.exists(config.model_save_dir):
        os.makedirs(config.model_save_dir)
    if not os.path.exists(config.sampling_dir):
        os.makedirs(config.sampling_dir)

    # Data loader.
    vcc_loader = get_loader(config.data_dir, config.batch_size, config.len_crop)
    
    solver = Solver(vcc_loader, config)

    solver.train()
        
    
        

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # Model configuration.
    parser.add_argument('--dim_emb', type=int, default=256)
    parser.add_argument('--dim_pre', type=int, default=512)
    parser.add_argument('--len_min', type=int, default=128)
    
    # Training configuration.
    parser.add_argument('--data_dir', type=str, default='./spmel/VCTK_train')
    parser.add_argument('--demo_file', type=str, default='./spmel/valid_b8/demo.pkl')
    parser.add_argument('--batch_size', type=int, default=64, help='mini-batch size')
    parser.add_argument('--num_iters', type=int, default=1000001, help='number of total iterations')
    parser.add_argument('--len_crop', type=int, default=128, help='dataloader output sequence length')
    parser.add_argument('--resume_iters', type=int, default=None, help='resume training from this step')
    os.environ["CUDA_VISIBLE_DEVICES"] = '3'
    parser.add_argument('--device_id', type=int, default=0)

    # Directories.
    model_dir = 'run_frm_convt_resampling'
    parser.add_argument('--model_save_dir', type=str, default=os.path.join(model_dir, 'models'))
    parser.add_argument('--sampling_dir', type=str, default=os.path.join(model_dir, 'samplings'))

    # Miscellaneous.
    parser.add_argument('--log_step', type=int, default=100)
    parser.add_argument('--model_save_step', type=int, default=10000)
    parser.add_argument('--sampling_step', type=int, default=10000)

    config = parser.parse_args()
    print(config)
    main(config)
