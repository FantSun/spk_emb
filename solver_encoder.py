import os
import numpy as np
import torch
import torch.nn.functional as F
import time
import datetime
import pickle
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from model_vc import Generator
from model_vc import RCR_segment
from model_vc import RCR_frame

class Solver(object):

    def __init__(self, vcc_loader, config):
        """Initialize configurations."""

        # Data loader.
        self.vcc_loader = vcc_loader

        # Model configurations.
        self.dim_emb = config.dim_emb
        self.dim_pre = config.dim_pre
        self.len_min = config.len_min

        # Training configurations.
        self.batch_size = config.batch_size
        self.num_iters = config.num_iters
        
        # Miscellaneous.
        self.use_cuda = torch.cuda.is_available()
        self.device = torch.device('cuda:{}'.format(config.device_id) if self.use_cuda else 'cpu')
        self.log_step = config.log_step
        self.model_save_step = config.model_save_step
        self.sampling_step = config.sampling_step

        # Directories.
        self.model_save_dir = config.model_save_dir
        self.sampling_dir = config.sampling_dir
        self.validation_pt = pickle.load(open(config.demo_file, "rb"))

        # Build the model and tensorboard.
        self.build_model()

            
    def build_model(self):
        
        self.G = Generator(self.dim_emb, self.dim_pre, self.len_min)        
        
        self.g_optimizer = torch.optim.Adam(self.G.parameters(), 0.0001)
        
        self.G.to(self.device)
        

    def restore_model(self, resume_iters):
        print('Loading the trained models from step {}...'.format(resume_iters))
        G_path = os.path.join(self.model_save_dir, '{}-G.ckpt'.format(resume_iters))
        g_checkpoint = torch.load(G_path, map_location=lambda storage, loc: storage)
        self.G.load_state_dict(g_checkpoint['model'])
        self.g_optimizer.load_state_dict(g_checkpoint['optimizer'])


    def reset_grad(self):
        """Reset the gradient buffers."""
        self.g_optimizer.zero_grad()
      
    
    #=====================================================================================================================================#
    
    
                
    def train(self):
        # Set data loader.
        data_loader = self.vcc_loader
        
        # Print logs in specified order
        keys = ['G/loss_id']
            
        # Start training.
        print('Start training...')
        start_time = time.time()
        for i in range(self.num_iters):

            # =================================================================================== #
            #                             1. Preprocess input data                                #
            # =================================================================================== #

            # Fetch data.
            try:
                x_real, _ = next(data_iter)
            except:
                data_iter = iter(data_loader)
                x_real, _ = next(data_iter)
            
            
            x_real = x_real.to(self.device) 
                        
       
            # =================================================================================== #
            #                               2. Train the generator                                #
            # =================================================================================== #
            
            self.G = self.G.train()
                        
            # Identity mapping loss
            #x_identic, _ = self.G(RCR_segment(x_real))
            x_identic, _ = self.G(RCR_frame(x_real))
            g_loss_id = F.mse_loss(x_real, x_identic)   

            # Backward and optimize.
            g_loss = g_loss_id
            self.reset_grad()
            g_loss.backward()
            self.g_optimizer.step()

            # Logging.
            loss = {}
            loss['G/loss_id'] = g_loss_id.item()

            # =================================================================================== #
            #                                 4. Miscellaneous                                    #
            # =================================================================================== #

            # Print out training information.
            if (i+1) % self.log_step == 0:
                et = time.time() - start_time
                et = str(datetime.timedelta(seconds=et))[:-7]
                log = "Elapsed [{}], Iteration [{}/{}]".format(et, i+1, self.num_iters)
                for tag in keys:
                    log += ", {}: {:.6f}".format(tag, loss[tag])
                print(log)

            # Save model checkpoints.
            if (i+1) % self.model_save_step == 0:
                G_path = os.path.join(self.model_save_dir, '{}-G.ckpt'.format(i+1))
                torch.save({'model': self.G.state_dict(),
                            'optimizer': self.g_optimizer.state_dict()}, G_path)
                print('Saved model checkpoints into {}...'.format(self.model_save_dir))

            # Paint t-SNE
            if (i+1) % self.sampling_step == 0:
                self.G = self.G.eval()
                cs = []
                ss = []
                with torch.no_grad():
                    for j, val_sub in enumerate(self.validation_pt):
                        for x_val in val_sub:
                            if len(x_val) // self.len_min > 0:
                                csp = []
                                for k in range(len(x_val) // self.len_min):
                                    uttr_val = torch.from_numpy(x_val[np.newaxis, k*self.len_min:(k+1)*self.len_min, :]).to(self.device)
                                    _, code_val = self.G(uttr_val)
                                    code_val = code_val.cpu().numpy()
                                    csp.append(code_val.reshape(1, -1))
                                cs.append(np.array(csp).mean(axis=0).squeeze())
                                ss.append(j)
                ts = TSNE(n_components=2).fit_transform(cs)
                plt.scatter(ts[:, 0], ts[:, 1], c=ss, alpha=0.5)
                plt.savefig(os.path.join(self.sampling_dir, 'tsne_' + str(i+1) + '.png'), dpi=150)
                plt.close()
                print('Saved t-SNE into {}...'.format(self.sampling_dir))
