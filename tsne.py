import os
import numpy as np
import torch
import pickle
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from model_vc import Generator

demo_file = './spmel/valid_b8/demo.pkl'
validation_pt = pickle.load(open(demo_file, "rb"))
model_save_dir = 'run_frm_convt'
sampling_dir = os.path.join('ocms/tsne', model_save_dir)
iter_model = 180000
device = torch.device('cuda:0')

len_min = 128
G = Generator(256, 512, len_min)
G.to(device)
G_path = os.path.join(model_save_dir, 'models','{}-G.ckpt'.format(iter_model))
g_checkpoint = torch.load(G_path, map_location=lambda storage, loc: storage)
G.load_state_dict(g_checkpoint['model'])
G = G.eval()

if not os.path.exists(sampling_dir):
    os.makedirs(sampling_dir)

cs = []
ss = []
with torch.no_grad():
    for j, val_sub in enumerate(validation_pt):
        for x_val in val_sub:
            if len(x_val) // len_min > 0:
                csp = []
                for k in range(len(x_val) // len_min):
                    uttr_val = torch.from_numpy(x_val[np.newaxis, k*len_min:(k+1)*len_min, :]).to(device)
                    _, code_val = G(uttr_val)
                    code_val = code_val.cpu().numpy()
                    csp.append(code_val.reshape(1, -1))
                cs.append(np.array(csp).mean(axis=0).squeeze())
                ss.append(j)
ts = TSNE(n_components=2).fit_transform(cs)
plt.scatter(ts[:, 0], ts[:, 1], c=ss, alpha=0.5)
plt.savefig(os.path.join(sampling_dir, 'tsne_' + str(iter_model) + '.png'), dpi=150)
print('Saved t-SNE into {}...'.format(sampling_dir))
